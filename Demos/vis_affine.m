v = random('normal',1,eps,[12,1]);
rdv = pdist(v);
rsa.fig.showRDMs(rdv,1,0);
rsa.fig.showRDMs(rdv,2,1,[0 1]);

%%
clear
n = 8;
x = randn(n,2);
rdm = squareform(pdist(x));

figure; 
% scatter3(x(:,1),x(:,2),x(:,3),[],'k.')
scatter(x(:,1),x(:,2),[],'k.')

% hold on

for i=1:5
    theta = 180*rand;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    y = x*R;
    gplot(ones(n),y);
%     y = x*rotx(180*rand(1))*roty(180*rand(1))*rotz(180*rand(1));
%     h = scatter3(y(:,1),y(:,2),y(:,3),[],'r.');
%     axis([-3 3 -3 3 -3 3])
    pause(.5)
%     delete(h)
end
    
    
